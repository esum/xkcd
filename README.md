`xkcd.py` : Télécharge les pages wiki de explainxkcd.com pour tous les xkcd (peut être assez lent, n'hésitez pas à réduire la taille de la pool de sous-processus dans ce cas) et les place dans le dossier `files/`.

`parser.py` : Parse les pages de explainxkcd.com téléchargées par `xkcd.py` et met leur contenu au format json dans le dossier `json/`.

`get_xkcd.py content` : Cherche un xkcd pertinent contenant `content`.
