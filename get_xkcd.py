#!/usr/bin/python3

import json
import os
import re
import sys


if __name__ == '__main__':
	xkcds = {}
	for xkcd in os.listdir('json'):
		with open('json' + os.path.sep + xkcd) as f:
			j = json.load(f)
			xkcds[int(j['number'])] = j
	t = ' '.join(sys.argv[1:]).casefold()
	scores = {}
	max_score = 0
	winner = 0
	for n in xkcds:
		xkcd = xkcds[n]
		scores[n] = 0
		if t in xkcd['title'].casefold():
			scores[n] += 100
			if re.search(r'\b' + t + r'\b', xkcd['title'].casefold()):
				scores[n] += 50
		if t in xkcd['titletext'].casefold():
			scores[n] += 20
			if re.search(r'\b' + t + r'\b', xkcd['titletext'].casefold()):
				scores[n] += 10
		if t in xkcd['transcript'].casefold():
			scores[n] += 30
			if re.search(r'\b' + t + r'\b', xkcd['transcript'].casefold()):
				scores[n] += 30
		if scores[n] > max_score:
			winner = n
			max_score = scores[n]
	if winner:
		print(xkcds[winner]['number'], xkcds[winner]['title'], 'avec', max_score)
	else:
		print('Rien trouvé :(')
