#!/usr/bin/python3

import json
import os
import re
import sys

NO_TRANSCRIPT = {1116}

def parse(file):
	with open(file) as f:
		page = f.read()
	try:
		page = re.sub(r'<!--.*?-->', '', page, flags=re.S)
		number = re.search(r'\{\{[Cc]omic.*number\s*=\s*(\d+)\s*\n.*\}\}', page, flags=re.S).group(1)
		title = re.search(r'\{\{[Cc]omic.*title\s*=\s*([^\n]+)\s*\n.*\}\}', page, flags=re.S).group(1)
		try:
			titletext = re.search(r'\{\{[Cc]omic.*titletext\s*=\s*([^\n]+)\s*\n.*\}\}', page, flags=re.S).group(1)
		except:
			titletext = ''
		transcript = re.search(r'\n==\s*(?:Transcript|~\*~TrAnScRiPt~\*~)\s*==\n(.*)\n==\S+==\n', page, flags=re.S)
		if transcript is None:
			transcript = re.search(r'\n==\s*(?:Transcript|~\*~TrAnScRiPt~\*~)\s*==\n(.*)\n:?\{\{.*\}\}', page, flags=re.S)
		try:
			transcript = transcript.group(1)
		except Exception as e:
			if int(file[6:-4]) in NO_TRANSCRIPT:
				transcript = ''
			else:
				raise e
		with open('json' + os.path.sep + file[6:-4]+'.json', 'w') as f:
			json.dump({'number': number, 'title': title, 'titletext': titletext, 'transcript': transcript}, f)
	except Exception as e:
		print(page, file=sys.stderr)
		raise e

if __name__ == '__main__':
	try:
		os.mkdir('json')
	except:
		pass
	for file in os.listdir('files'):
		parse('files' + os.path.sep + file)

