#!/usr/bin/python3

from multiprocessing import Pool
import os
import re
import sys
import urllib.request

# Les méchants sites préfèrent qu'on mette ça dans les requêtes sinon ils 403…
HTML_HEADER = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko)Chrome/35.0.1916.47 Safari/537.36'}

try:
	req = urllib.request.Request('https://xkcd.com', data=None, headers=HTML_HEADER)
	response = urllib.request.urlopen(req)
	html = response.read()
	m = re.search(rb'Permanent link to this comic: https://xkcd.com/(\d+)/', html)
	LAST_XKCD = int(m.group(1).decode('ascii'))
except:
	LAST_XKCD = 2000


def get_xkcd(xkcd):
	try:
		n = xkcd
		req = urllib.request.Request('https://www.explainxkcd.com/wiki/index.php/{xkcd}?action=raw'.format(xkcd=xkcd), data=None, headers=HTML_HEADER)
		response = urllib.request.urlopen(req)
		html = response.read()
		m = re.search(rb'#[Rr][Ee][Dd][Ii][Rr][Ee][Cc][Tt]:?\s*\[\[(.*)\]\]', html)
		xkcd = m.group(1).decode('utf-8', 'ignore').replace('?', '%3F').replace('é', '%C3%A9').replace(' ', '%20')
		xkcd = xkcd.encode('ascii', 'ignore').decode('ascii')
		req = urllib.request.Request('https://www.explainxkcd.com/wiki/index.php/{xkcd}?action=raw'.format(xkcd=xkcd), data=None, headers=HTML_HEADER)
		response = urllib.request.urlopen(req)
		html = response.read()
		with open('files/{n}.txt'.format(n=n), 'wb') as file:
			file.write(html)
	except Exception as e:
		print('Error with xkcd #', xkcd, e, file=sys.stderr)


if __name__ == '__main__':
	try:
		os.mkdir('files')
	except:
		pass
	with Pool(os.cpu_count()*2) as pool:
		pool.map(get_xkcd, range(1, LAST_XKCD+1))
